#!/bin/bash

# Run the database server first.
# Ensure that the URL uses an ip address the container can reach.
#
# The --mount must point to the directory that contains the secrets.

docker run --rm --name ebms-embedded-test \
    -p 8080:8080 \
    --mount "type=bind,source=/home/peza/DevProjects/ebms-admin-docker/secrets,destination=/app/secrets,readonly" \
    --env EBMS_SOAP=Y \
    --env EBMS_START_EMBEDDED=Y \
    --env EBMS_JDBC_DRIVER_CLASS_NAME=org.postgresql.Driver \
    --env EBMS_JDBC_USERNAME=ebms \
    --env EBMS_JDBC_PASSWORD=secretPassword \
    --env EBMS_JDBC_URL=jdbc:postgresql://10.0.2.15:5432/ebms \
    --env EBMS_POOL_PREFERRED_TEST_QUERY="select 1" \
    ebms-admin:postgres

