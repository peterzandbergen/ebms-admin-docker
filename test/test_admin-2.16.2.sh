#!/bin/bash

# The --mount must point to the directory that contains the secrets.

docker run --rm --name ebms-admin-test \
    -p 8081:8080 \
    --env EBMS_SERVICE_URL="http://10.0.2.15:8080/service" \
    peterzandbergen/ebms-admin:2.16.2

    # --env EBMS_ADMIN_SOAP=Y \
    # --env EBMS_ADMIN_HSQLDB=Y \
    # --env EBMS_ADMIN_HSQLDB_DIR=/app/localdb \
    # --mount "type=bind,source=/home/peza/DevProjects/ebms-admin-docker/secrets,destination=/app/secrets,readonly" \
    # --mount 'type=volume,source=ebms-localdb,destination=/app/localdb' \
    # --env EBMS_ADMIN_CLIENT_AUTHENTICATION=Y \
    # --env EBMS_ADMIN_AUTHENTICATION=Y \
    # --env EBMS_ADMIN_KEY_STORE_PASSWORD=password \
    # --env EBMS_ADMIN_KEY_STORE_PATH=/app/secrets/admin-keystore.jks \
    # --env EBMS_ADMIN_KEY_STORE_TYPE=JKS \
    # --env EBMS_ADMIN_TRUST_STORE_PASSWORD=password \
    # --env EBMS_ADMIN_TRUST_STORE_PATH=/app/secrets/admin-truststore.jks \
    # --env EBMS_ADMIN_TRUST_STORE_TYPE=JKS \
    # --env EBMS_ADMIN_CLIENT_TRUST_STORE_PASSWORD=password  \
    # --env EBMS_ADMIN_CLIENT_TRUST_STORE_PATH=/app/secrets/admin-client-truststore.jks  \
    # --env EBMS_ADMIN_CLIENT_TRUST_STORE_TYPE=JKS  \
    # --env EBMS_KEYSTORE_PASSWORD=password \
    # --env EBMS_KEYSTORE_PATH=/app/secrets/keystore.jks \
    # --env EBMS_KEYSTORE_TYPE=JKS \
    # --env EBMS_CLIENT_KEYSTORE_PASSWORD=password \
    # --env EBMS_CLIENT_KEYSTORE_PATH=/app/secrets/client-keystore.jks \
    # --env EBMS_CLIENT_KEYSTORE_TYPE=JKS \
    # --env EBMS_SIGNATURE_KEYSTORE_PASSWORD=password \
    # --env EBMS_SIGNATURE_KEYSTORE_PATH=/app/secrets/signature-keystore.jks \
    # --env EBMS_SIGNATURE_KEYSTORE_TYPE=JKS \
    # --env EBMS_ENCRYPTION_KEYSTORE_PASSWORD=password \
    # --env EBMS_ENCRYPTION_KEYSTORE_PATH=/app/secrets/encryption-keystore.jks \
    # --env EBMS_ENCRYPTION_KEYSTORE_TYPE=JKS \
    # --env EBMS_TRUSTSTORE_PASSWORD=password \
    # --env EBMS_TRUSTSTORE_PATH=/app/secrets/truststore.jks \
    # --env EBMS_TRUSTSTORE_TYPE=JKS \
    # --env EBMS_SSL=false \
    # --env EBMS_LOG4J_FILE=file:/app/conf/log4j2.xml \
    # --env EBMS_HTTP_CLIENT=DEFAULT \
    # --env EBMS_JMS_BROKER_START=true \
    # --env EBMS_ADMIN_PROPERTIES_FILE_DIR=/app/conf \
    # --env EBMS_ADMIN_HEADLESS=false \
    # --env EBMS_PATH=/ebms \
    # --env EBMS_SSL=true \
    # --env EBMS_HOST=0.0.0.0 \
    # --env EBMS_ADMIN_SSL=true \
    # --env EBMS_ADMIN_PATH=/admin_path \