#!/bin/bash

# The --mount must point to the directory that contains the secrets.

docker run --rm --name ebms-embedded-test-$1 \
    -p 8080:8080 \
    -p 8888:8888 \
    --env EBMS_ADMIN_SOAP=Y \
    --env EBMS_START_EMBEDDED=Y \
    --env EBMS_ADMIN_HSQLDB=Y \
    --env EBMS_ADMIN_HSQLDB_DIR=/app/localdb \
    --mount 'type=volume,source=embs-localdb,destination=/app/localdb' \
    --env EBMS_ADMIN_SOAP=true \
    peterzandbergen/ebms-admin:$1

    # -p 8080:8080 \
    # -p 8443:8443 \
    # --env EBMS_ADMIN_PROPERTIES_FILE_DIR=/app/conf/weg/ \
    # --env EBMS_ADMIN_HEADLESS=false \
    # --env EBMS_PATH=/ebms \
    # --env EBMS_SSL=true \
    # --env EBMS_HOST=0.0.0.0 \
    # --env EBMS_ADMIN_SSL=true \
    # --env EBMS_ADMIN_PATH=/admin_path \
    # --env EBMS_ADMIN_PORT=8889 \
    