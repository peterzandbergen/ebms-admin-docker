#!/bin/bash

#!/bin/bash

# The --mount must point to the directory that contains the secrets.

docker run --rm --name ebms-digipoort \
    -p 18080:8080 \
    -p 18443:8443 \
    -p 18888:8888 \
    --env EBMS_ADMIN_SOAP=Y \
    --env EBMS_START_EMBEDDED=Y \
    --env EBMS_ADMIN_HSQLDB=Y \
    --env EBMS_ADMIN_HSQLDB_DIR=/app/localdb \
    --env EBMS_SSL=false \
    --env EBMS_PATH=/digipoortStub \
    --env EBMS_EVENT_LISTENER_TYPE=DAO \
    --mount 'type=volume,source=embs-localdb-digipoort,destination=/app/localdb' \
    peterzandbergen/ebms-admin:2.16.6

    # --env EBMS_ADMIN_HEADLESS=false \
    # --env EBMS_PATH=/ebms \
    # --env EBMS_SSL=true \
    # --env EBMS_HOST=0.0.0.0 \
    # --env EBMS_ADMIN_SSL=true \
    # --env EBMS_ADMIN_PATH=/admin_path \
    # --env EBMS_ADMIN_HOST=10.0.2.15 \
    # --env EBMS_HOST=0.0.0.0 \
