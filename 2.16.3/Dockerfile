# Build with:
# docker build --tag "ebms-admin:tag" .
# peterzandbergen/ebms-admin:2.16.3
FROM openjdk:8-jre-slim

RUN set -eux; \
	apt-get update; \
	apt-get install -y gosu; \
	rm -rf /var/lib/apt/lists/*; \
# verify that the binary works
	gosu nobody true

# Default ports for the ebms service 8888 and web interface 8080
EXPOSE 8080

COPY ebms-admin-2.16.3.jar          /app/jar/
COPY log4j2.xml                      /app/conf/
COPY 2.16.3/entrypoint.sh           /app/bin/
# Default keystores for easy testing.
COPY secrets/keystore.jks            /app/secrets/
COPY secrets/truststore.jks          /app/secrets/

# Create non root account.
RUN set -eux; \
    groupadd --system appuser --gid=999; \
    useradd  --system --gid appuser --uid=999 appuser;

RUN mkdir -p /app/logs

WORKDIR /app/conf

# Set the ebms jar.
ENV EBMS_ADMIN_JAR_PATH=/app/jar/ebms-admin-2.16.3.jar
# Set default log file.
ENV EBMS_LOG4J_FILE=file:/app/conf/log4j2.xml

CMD [  ]

# entrypoint.sh takes all parameters from the 
ENTRYPOINT [ "/app/bin/entrypoint.sh" ]
