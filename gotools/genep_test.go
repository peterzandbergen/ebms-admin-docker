package gotools

import "testing"

var propTestCases = []struct{ p, e, t string }{
	{"https.requireSSLAuthentication", "EBMS_HTTPS_REQUIRE_SSL_AUTHENTICATION", "PROPERTY"},
	{"ebms.ssl", "EBMS_SSL", "PROPERTY"},
	{"ebms.jdbc.driverClassName", "EBMS_JDBC_DRIVER_CLASS_NAME", "PROPERTY"},
	{"https.verifyHostnames", "EBMS_HTTPS_VERIFY_HOSTNAMES", "PROPERTY"},
	{"+authentication", "EBMS_ADMIN_AUTHENTICATION", "CLI_FLAG"},
	{"+ssl", "EBMS_ADMIN_SSL", "CLI_FLAG"},
	{"-clientCertificateHeader", "EBMS_ADMIN_CLIENT_CERTIFICATE_HEADER", "CLI_PARAM"},
	{"jms.brokerURL", "EBMS_JMS_BROKER_URL","PROPERTY"},
	{"jms.brokerURL", "EBMS_JMS_BROKER_URL","PROPERTY"},
}

func TestBuildParam(t *testing.T) {
	for _, c := range propTestCases {
		param := buildParam(c.p)
		if param.EnvName != c.e {
			t.Errorf("EnvName: expected %s, got %s", c.t, param.EnvName)
		}
		if param.PType != c.t {
			t.Errorf("PType: expected %s, got %s", c.t, param.PType)
		}
	}
}


// func testWithString() {
// 	ps := buildParams(scanParams(strings.NewReader(params)))
// 	for _, p := range ps {
// 		fmt.Printf("%v\n", p)
// 	}
// }

