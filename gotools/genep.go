package gotools

import (
	"bufio"
	"bytes"

	// "encoding/csv"
	// "fmt"
	"io"
	// "log"
	// "os"
	// "strings"
	"unicode"
)

const (
	params = `-authentication
-clientAuthentication
-clientCertificateHeader
-clientTrustStorePassword
-clientTrustStorePath
-clientTrustStoreType
-headless
-host
-hsqldb
-hsqldbDir
-jmx
-keyStorePassword
-keyStorePath
-keyStoreType
-path
-port
-soap
-ssl
-trustStorePassword
-trustStorePath
-trustStoreType
cache.disabled
cache.maxElementsInMemory
cache.maxElementsOnDisk
cache.memoryStoreEvictionPolicy
cache.overflowToDisk
cache.timeToIdle
cache.timeToLive
client.keystore.password
client.keystore.path
client.keystore.type
deliveryManager.maxTreads
deliveryManager.processorsScaleFactor
deliveryManager.queueScaleFactor
ebms.allowMultipleServers
ebms.host
ebms.jdbc.driverClassName
ebms.jdbc.password
ebms.jdbc.url
ebms.jdbc.username
ebms.path
ebms.pool.acquireIncrement
ebms.pool.acquireRetryAttempts
ebms.pool.acquireRetryDelay
ebms.pool.autoCommitOnClose
ebms.pool.automaticTestTable
ebms.pool.breakAfterAcquireFailure
ebms.pool.checkoutTimeout
ebms.pool.connectionCustomizerClassName
ebms.pool.connectionTesterClassName
ebms.pool.debugUnreturnedConnectionStackTraces
ebms.pool.factoryClassLocation
ebms.pool.forceIgnoreUnresolvedTransactions
ebms.pool.idleConnectionTestPeriod
ebms.pool.initialPoolSize
ebms.pool.maxAdministrativeTaskTime
ebms.pool.maxConnectionAge
ebms.pool.maxIdleTime
ebms.pool.maxIdleTimeExcessConnections
ebms.pool.maxPoolSize
ebms.pool.maxStatements
ebms.pool.maxStatementsPerConnection
ebms.pool.minPoolSize
ebms.pool.numHelperThreads
ebms.pool.preferredTestQuery
ebms.pool.testConnectionOnCheckin
ebms.pool.testConnectionOnCheckout
ebms.pool.unreturnedConnectionTimeout
ebms.pool.usesTraditionalReflectiveProxies
ebms.port
ebms.serverId
ebms.ssl
ebmsMessage.autoRetryInterval
ebmsMessage.autoRetryResponse
ebmsMessage.deleteContentOnProcessed
ebmsMessage.nrAutoRetries
ebmsMessage.storeDuplicate
ebmsMessage.storeDuplicateContent
encryption.keystore.password
encryption.keystore.path
encryption.keystore.type
eventListener.type
http.base64Writer
http.chunkedStreamingMode
http.client
http.errors.client.recoverable
http.errors.informational.recoverable
http.errors.redirection.recoverable
http.errors.server.irrecoverable
http.proxy.host
http.proxy.nonProxyHosts
http.proxy.password
http.proxy.port
http.proxy.username
https.cipherSuites
https.clientCertificateAuthentication
https.protocols
https.requireClientAuthentication
https.requireSSLAuthentication
https.verifyHostnames
jms.broker.config
jms.broker.start
jms.brokerURL
jms.virtualTopics
job.maxTreads
job.processorsScaleFactor
job.queueScaleFactor
jobScheduler.delay
jobScheduler.enabled
jobScheduler.period
keystore.password
keystore.path
keystore.type
log4j.file
maxItemsPerPage
messageQueue.maxEntries
messageQueue.timeout
patch.cleo.enable
patch.digipoort.enable
patch.oracle.enable
service.ebms.url
signature.keystore.password
signature.keystore.path
signature.keystore.type
truststore.password
truststore.path
truststore.type
xmldsig.canonicalizationMethodAlgorithm
xmldsig.transformAlgorithm
`
)

// EbmsParameter contains parameters to generate the entrypoint setting.
type EbmsParameter struct {
	PType         string
	EnvName       string
	EbmsParameter string
	Default       string
	Description   string
}

func writeEnvVar(b *bytes.Buffer, s string) {
	rs := []rune(s)
	for i, c := range rs {
		if i == 0 {
			b.WriteRune(unicode.ToUpper(c))
			continue
		}
		if i == len(rs)-1 {
			b.WriteRune(unicode.ToUpper(c))
			continue
		}
		if unicode.IsUpper(c) {
			if unicode.IsLower(rs[i-1]) {
				b.WriteRune('_')
				b.WriteRune(c)
				continue
			}
			if unicode.IsLower(rs[i+1]) {
				b.WriteRune('_')
				b.WriteRune(c)
				continue
			}
		}
		if c == '.' {
			b.WriteRune('_')
			continue
		}
		b.WriteRune(unicode.ToUpper(c))

	}
}

func buildCliParam(s string) *EbmsParameter {
	var b bytes.Buffer

	b.WriteString("EBMS_ADMIN_")
	writeEnvVar(&b, s[1:])
	if s[0] == '-' {
		return &EbmsParameter{
			PType:         "CLI_PARAM",
			EnvName:       b.String(),
			EbmsParameter: s,
		}
	}
	return &EbmsParameter{
		PType:         "CLI_FLAG",
		EnvName:       b.String(),
		EbmsParameter: "-" + s[1:],
	}
}

func buildProperty(s string) *EbmsParameter {
	var b bytes.Buffer
	const pf = "ebms."
	var pflen = 5
	if s[:pflen] == pf {
		s = s[pflen:]
	}

	b.WriteString("EBMS_")
	writeEnvVar(&b, s)
	return &EbmsParameter{
		PType:         "PROPERTY",
		EnvName:       b.String(),
		EbmsParameter: s,
	}
}

func scanParams(r io.Reader) []string {
	var res []string
	s := bufio.NewScanner(r)
	for s.Scan() {
		res = append(res, s.Text())
	}
	return res
}

func buildParam(s string) *EbmsParameter {
	if len(s) == 0 {
		return nil
	}
	if s[0] == '-' || s[0] == '+' {
		return buildCliParam(s)
	}
	return buildProperty(s)
}

func buildParams(lines []string) []*EbmsParameter {
	var res []*EbmsParameter
	for _, s := range lines {
		if p := buildParam(s); p != nil {
			res = append(res, p)
		}
	}
	return res
}

func isCliParam(s string) bool {
	return len(s) > 0 && (s[0] == '-' || s[0] == '+')
}

func isProperty(s string) bool {
	return !isCliParam(s)
}

func BuildFromRow(row []string) *EbmsParameter {
	p := buildParam(row[2])
	if len(p.PType) == 0 {
		p.PType = row[0]
	}
	p.Default = row[3]
	p.Description = row[4]
	return p
}
