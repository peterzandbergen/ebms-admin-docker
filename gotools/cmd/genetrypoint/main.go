package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"

	"gotools"
)

func testWithCsv(filename string) {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatalf("error opening file: %s", err.Error())
	}
	defer f.Close()
	c := csv.NewReader(f)
	for {
		row, err := c.Read()
		if err != nil {
			break
		}
		p := gotools.BuildFromRow(row)
		fmt.Printf("%v\n", p)
	}
}

func main() {
	testWithCsv("param-list.csv")
}
