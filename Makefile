.PHONY: all push clean 

all: push

all-images: base-images postgres-images ebms-database-postgres-image

push: pushed-images

base-images: base-image-2.16.6 base-image-2.16.5 base-image-2.16.4 base-image-2.16.3 base-image-2.16.1 base-image-2.16.2 base-image-2.16.2a

postgres-images: postgres-image-2.16.1 postgres-image-2.16.2 postgres-image-2.16.2a postgres-image-2.16.3 postgres-image-2.16.4 postgres-image-2.16.5 postgres-image-2.16.6

pushed-images: pushed-images-2.16.1 pushed-images-2.16.2 pushed-images-2.16.2a pushed-images-2.16.3 pushed-images-2.16.4 pushed-images-2.16.5 pushed-images-2.16.6 pushed-ebms-database-postgres-image

pushed-images-2.16.6: base-image-2.16.6 postgres-image-2.16.6 
	docker push peterzandbergen/ebms-admin:latest
	docker push peterzandbergen/ebms-admin:postgres
	docker push peterzandbergen/ebms-admin:2.16.6
	docker push peterzandbergen/ebms-admin:2.16.6-postgres
	echo "DONE" > pushed-images-2.16.6

pushed-images-2.16.5: base-image-2.16.5 postgres-image-2.16.5 
	docker push peterzandbergen/ebms-admin:2.16.5
	docker push peterzandbergen/ebms-admin:2.16.5-postgres
	echo "DONE" > pushed-images-2.16.5

pushed-images-2.16.4: base-image-2.16.4 postgres-image-2.16.4 
	docker push peterzandbergen/ebms-admin:2.16.4
	docker push peterzandbergen/ebms-admin:2.16.4-postgres
	echo "DONE" > pushed-images-2.16.4

pushed-images-2.16.3: base-image-2.16.3 postgres-image-2.16.3 
	docker push peterzandbergen/ebms-admin:2.16.3
	docker push peterzandbergen/ebms-admin:2.16.3-postgres
	echo "DONE" > pushed-images-2.16.3

pushed-images-2.16.2a: base-image-2.16.2a postgres-image-2.16.2a 
	docker push peterzandbergen/ebms-admin:2.16.2a
	docker push peterzandbergen/ebms-admin:2.16.2a-postgres
	echo "DONE" > pushed-images-2.16.2a

pushed-images-2.16.2: base-image-2.16.2 postgres-image-2.16.2 
	docker push peterzandbergen/ebms-admin:2.16.2
	docker push peterzandbergen/ebms-admin:2.16.2-postgres
	echo "DONE" > pushed-images-2.16.2

pushed-images-2.16.1: base-image-2.16.1 postgres-image-2.16.1
	docker push peterzandbergen/ebms-admin:2.16.1
	docker push peterzandbergen/ebms-admin:2.16.1-postgres
	echo "DONE" > pushed-images-2.16.1

pushed-ebms-database-postgres-image: ebms-database-postgres-image
	docker push peterzandbergen/ebms-database-postgres:latest
	docker push peterzandbergen/ebms-database-postgres:12
	echo "DONE" > pushed-ebms-database-postgres-image

base-image-2.16.6 : 2.16.6/Dockerfile 2.16.6/entrypoint.sh log4j2.xml ebms-admin-2.16.6.jar
	docker build --file 2.16.6/Dockerfile --tag ebms-admin:2.16.6 .
	docker tag ebms-admin:2.16.6 peterzandbergen/ebms-admin:2.16.6
	docker tag ebms-admin:2.16.6 ebms-admin:latest
	docker tag ebms-admin:2.16.6 peterzandbergen/ebms-admin:latest
	echo "DONE" > base-image-2.16.6

base-image-2.16.5 : 2.16.5/Dockerfile 2.16.5/entrypoint.sh log4j2.xml ebms-admin-2.16.5.jar
	docker build --file 2.16.5/Dockerfile --tag ebms-admin:2.16.5 .
	docker tag ebms-admin:2.16.5 peterzandbergen/ebms-admin:2.16.5
	echo "DONE" > base-image-2.16.5

base-image-2.16.4 : 2.16.4/Dockerfile 2.16.4/entrypoint.sh log4j2.xml ebms-admin-2.16.4.jar
	docker build --file 2.16.4/Dockerfile --tag ebms-admin:2.16.4 .
	docker tag ebms-admin:2.16.4 peterzandbergen/ebms-admin:2.16.4
	echo "DONE" > base-image-2.16.4

base-image-2.16.3 : 2.16.3/Dockerfile 2.16.3/entrypoint.sh log4j2.xml ebms-admin-2.16.3.jar
	docker build --file 2.16.3/Dockerfile --tag ebms-admin:2.16.3 .
	docker tag ebms-admin:2.16.3 peterzandbergen/ebms-admin:2.16.3
	echo "DONE" > base-image-2.16.3

base-image-2.16.2a : 2.16.2a/Dockerfile 2.16.2a/entrypoint.sh log4j2.xml ebms-admin-2.16.2a.jar
	docker build --file 2.16.2a/Dockerfile --tag ebms-admin:2.16.2a .
	docker tag ebms-admin:2.16.2a peterzandbergen/ebms-admin:2.16.2a
	echo "DONE" > base-image-2.16.2a

base-image-2.16.2 : 2.16.2/Dockerfile 2.16.2/entrypoint.sh log4j2.xml ebms-admin-2.16.2.jar
	docker build --file 2.16.2/Dockerfile --tag ebms-admin:2.16.2 .
	docker tag ebms-admin:2.16.2 peterzandbergen/ebms-admin:2.16.2
	echo "DONE" > base-image-2.16.2

base-image-2.16.1 : 2.16.1/Dockerfile 2.16.1/entrypoint.sh log4j2.xml ebms-admin-2.16.1.jar
	docker build --file 2.16.1/Dockerfile --tag ebms-admin:2.16.1 .
	docker tag ebms-admin:2.16.1 peterzandbergen/ebms-admin:2.16.1
	echo "DONE" > base-image-2.16.1

postgres-image-2.16.6: base-image-2.16.6 ebms-admin-postgres/2.16.6/Dockerfile postgresql-42.2.9.jar
	docker build --file ebms-admin-postgres/2.16.6/Dockerfile --tag ebms-admin:2.16.6-postgres .
	docker tag ebms-admin:2.16.6-postgres peterzandbergen/ebms-admin:2.16.6-postgres
	docker tag ebms-admin:2.16.6-postgres ebms-admin:postgres
	docker tag ebms-admin:2.16.6-postgres peterzandbergen/ebms-admin:postgres
	echo "DONE" > postgres-image-2.16.6

postgres-image-2.16.5: base-image-2.16.5 ebms-admin-postgres/2.16.5/Dockerfile postgresql-42.2.9.jar
	docker build --file ebms-admin-postgres/2.16.5/Dockerfile --tag ebms-admin:2.16.5-postgres .
	docker tag ebms-admin:2.16.5-postgres peterzandbergen/ebms-admin:2.16.5-postgres
	echo "DONE" > postgres-image-2.16.5

postgres-image-2.16.4: base-image-2.16.4 ebms-admin-postgres/2.16.4/Dockerfile postgresql-42.2.9.jar
	docker build --file ebms-admin-postgres/2.16.4/Dockerfile --tag ebms-admin:2.16.4-postgres .
	docker tag ebms-admin:2.16.4-postgres peterzandbergen/ebms-admin:2.16.4-postgres
	echo "DONE" > postgres-image-2.16.4

postgres-image-2.16.3: base-image-2.16.3 ebms-admin-postgres/2.16.3/Dockerfile postgresql-42.2.9.jar
	docker build --file ebms-admin-postgres/2.16.3/Dockerfile --tag ebms-admin:2.16.3-postgres .
	docker tag ebms-admin:2.16.3-postgres peterzandbergen/ebms-admin:2.16.3-postgres
	echo "DONE" > postgres-image-2.16.3

postgres-image-2.16.2a: base-image-2.16.2a ebms-admin-postgres/2.16.2a/Dockerfile postgresql-42.2.9.jar
	docker build --file ebms-admin-postgres/2.16.2a/Dockerfile --tag ebms-admin:2.16.2a-postgres .
	docker tag ebms-admin:2.16.2a-postgres peterzandbergen/ebms-admin:2.16.2a-postgres
	echo "DONE" > postgres-image-2.16.2a

postgres-image-2.16.2: base-image-2.16.2 ebms-admin-postgres/2.16.2/Dockerfile postgresql-42.2.9.jar
	docker build --file ebms-admin-postgres/2.16.2/Dockerfile --tag ebms-admin:2.16.2-postgres .
	docker tag ebms-admin:2.16.2-postgres peterzandbergen/ebms-admin:2.16.2-postgres
	echo "DONE" > postgres-image-2.16.2

postgres-image-2.16.1: base-image-2.16.1 ebms-admin-postgres/2.16.1/Dockerfile postgresql-42.2.9.jar
	docker build --file ebms-admin-postgres/2.16.1/Dockerfile --tag ebms-admin:2.16.1-postgres .
	docker tag ebms-admin:2.16.1-postgres peterzandbergen/ebms-admin:2.16.1-postgres
	echo "DONE" > postgres-image-2.16.1

postgresql-42.2.9.jar:
	curl https://jdbc.postgresql.org/download/postgresql-42.2.9.jar -o postgresql-42.2.9.jar

ebms-database-postgres-image: ebms-database-postgres/Dockerfile create-ebms-pg-database.sql
	docker build -f ebms-database-postgres/Dockerfile --tag ebms-database-postgres:12  .
	docker tag ebms-database-postgres:12 ebms-database-postgres:latest
	docker tag ebms-database-postgres:12 peterzandbergen/ebms-database-postgres:12
	docker tag ebms-database-postgres:12 peterzandbergen/ebms-database-postgres:latest
	echo "DONE" > ebms-database-postgres-image

# https://liquidtelecom.dl.sourceforge.net/project/javaebmsadmin/ebms-admin-2.16.5.jar
# https://liquidtelecom.dl.sourceforge.net/project/javaebmsadmin/ebms-admin-2.16.5.jar

DOWNLOAD_PREFIX="https://liquidtelecom.dl.sourceforge.net/project/javaebmsadmin"

download-ebms-all: ebms-admin-2.16.6.jar ebms-admin-2.16.5.jar ebms-admin-2.16.4.jar ebms-admin-2.16.3.jar ebms-admin-2.16.2a.jar ebms-admin-2.16.2.jar ebms-admin-2.16.1.jar

ebms-admin-2.16.6.jar:
	curl ${DOWNLOAD_PREFIX}/ebms-admin-2.16.6.jar -o ebms-admin-2.16.6.jar

ebms-admin-2.16.5.jar:
	curl ${DOWNLOAD_PREFIX}/ebms-admin-2.16.5.jar -o ebms-admin-2.16.5.jar

ebms-admin-2.16.4.jar:
	curl ${DOWNLOAD_PREFIX}/ebms-admin-2.16.4.jar -o ebms-admin-2.16.4.jar

ebms-admin-2.16.3.jar:
	curl ${DOWNLOAD_PREFIX}/ebms-admin-2.16.3.jar -o ebms-admin-2.16.3.jar

ebms-admin-2.16.2a.jar:
	curl ${DOWNLOAD_PREFIX}/ebms-admin-2.16.2a.jar -o ebms-admin-2.16.2a.jar

ebms-admin-2.16.2.jar:
	curl ${DOWNLOAD_PREFIX}/ebms-admin-2.16.2.jar -o ebms-admin-2.16.2.jar

ebms-admin-2.16.1.jar:
	curl ${DOWNLOAD_PREFIX}/ebms-admin-2.16.1.jar -o ebms-admin-2.16.1.jar

clean-images: clean-base-image clean-postgres-image

# .PHONY: clean
clean: clean
	# Clean postgres 2.16.6
	-docker rmi -f \
		ebms-admin:postgres \
		peterzandbergen/ebms-admin:postgres \
		ebms-admin:2.16.6-postgres \
		peterzandbergen/ebms-admin:2.16.6-postgres 
	-rm postgres-image-2.16.6
	# Clean postgres 2.16.5
	-docker rmi -f \
		ebms-admin:2.16.5-postgres \
		peterzandbergen/ebms-admin:2.16.5-postgres 
	-rm postgres-image-2.16.5
	# Clean postgres 2.16.4
	-docker rmi -f \
		ebms-admin:2.16.4-postgres \
		peterzandbergen/ebms-admin:2.16.4-postgres
	-rm postgres-image-2.16.4
	# Clean postgres 2.16.3
	-docker rmi -f \
		ebms-admin:2.16.3-postgres \
		peterzandbergen/ebms-admin:2.16.3-postgres
	-rm postgres-image-2.16.3
	# Clean postgres 2.16.2a
	-docker rmi -f \
		ebms-admin:2.16.2a-postgres \
		peterzandbergen/ebms-admin:2.16.2a-postgres
	-rm postgres-image-2.16.2a
	# Clean postgres 2.16.2
	-docker rmi -f \
		ebms-admin:2.16.2-postgres \
		peterzandbergen/ebms-admin:2.16.2-postgres
	-rm postgres-image-2.16.2
	# Clean postgres 2.16.1
	-docker rmi -f \
		ebms-admin:2.16.1-postgres \
		peterzandbergen/ebms-admin:2.16.1-postgres
	-rm postgres-image-2.16.1

	# Clean 2.16.6
	-docker rmi -f \
		ebms-admin:latest \
		peterzandbergen/ebms-admin:latest \
		ebms-admin:2.16.6 \
		peterzandbergen/ebms-admin:2.16.6
	-rm base-image-2.16.6
	# Clean 2.16.5
	-docker rmi -f \
		ebms-admin:latest \
		peterzandbergen/ebms-admin:latest \
		ebms-admin:2.16.5 \
		peterzandbergen/ebms-admin:2.16.5
	-rm base-image-2.16.5
	# Clean 2.16.4
	-docker rmi -f \
		ebms-admin:2.16.4 \
		peterzandbergen/ebms-admin:2.16.4
	-rm base-image-2.16.4
	# Clean 2.16.3
	-docker rmi -f \
		ebms-admin:2.16.3 \
		peterzandbergen/ebms-admin:2.16.3
	-rm base-image-2.16.3
	# Clean 2.16.2a
	-docker rmi -f \
		ebms-admin:2.16.2a \
		peterzandbergen/ebms-admin:2.16.2a
	-rm base-image-2.16.2a
	# Clean 2.16.2
	-docker rmi -f \
		ebms-admin:2.16.2 \
		peterzandbergen/ebms-admin:2.16.2 
	-rm base-image-2.16.2
	# Clean 2.16.1
	-docker rmi -f \
		ebms-admin:2.16.1 \
		peterzandbergen/ebms-admin:2.16.1 
	-rm base-image-2.16.1
	# Clean ebms postgres database
	-docker rmi -f \
		ebms-database-postgres:12 \
		ebms-database-postgres:latest \
		peterzandbergen/ebms-database-postgres:12 \
		peterzandbergen/ebms-database-postgres:latest
	-rm ebms-database-postgres-image
	# Clean the rest
	-rm pushed-images-2.16.6
	-rm pushed-images-2.16.5
	-rm pushed-images-2.16.4
	-rm pushed-images-2.16.3
	-rm pushed-images-2.16.2a
	-rm pushed-images-2.16.2
	-rm pushed-images-2.16.1
	-rm pushed-ebms-database-postgres-image
	-rm ebms-admin-2.16.6.jar
	-rm ebms-admin-2.16.5.jar
	-rm ebms-admin-2.16.4.jar
	-rm ebms-admin-2.16.3.jar
	-rm ebms-admin-2.16.2a.jar
	-rm ebms-admin-2.16.2.jar
	-rm ebms-admin-2.16.1.jar
	-rm postgresql-42.2.9.jar

