# embs-admin

Docker image for the ebms-admin version 2.16.1. This image can operate 
as an admin Console only, as an ebms-adapter only or as a combination of 
both. 
See [Java EbMS Admin](https://sourceforge.net/projects/javaebmsadmin/) on Source Forge 
for more information about the component.

The image follow the 12 factor recommendations and accepts the configuration from environment variables 
and logs to standard output.

The makefile creates several image with different tags for different 
purposes.

## Tags
- 2.16.3, latest
- 2.16.3-postgres, postgres
- 2.16.2a
- 2.16.2a-postgres
- 2.16.2
- 2.16.2-postgres
- 2.16.1
- 2.16.1-postgres

## Configuration

The standard ebms admin retrieves its configuration in three ways:

- command line parameters
- java properties file
- log4j2.xml logging configuration

The commandline parameters configure the settings for the admin web gui. The java properties file configures parameters of the ebms adapter and the adapter itself in the embedded mode. The log42.xml file configures the logging, including the log level.

To make the container image easy to use, the entrypoint is a bash script that reads the configuration from environment variables, convert them to cli parameters and modifices the java properties file.

## Configuration using environment variables
Instead of using the commandline parameters as described in 
[Java EbMS Admin](https://sourceforge.net/projects/javaebmsadmin/files/)
the image supports environment variables for the configuration of the 
admin console. The configuration of the adapter itself requires a properties file. Additionally the adapter needs keystores for server TLS certificate, for client TLS certificate and for the trusted certificates.

The environment variables follow the name of the command line parameters. The name is in upper case and capitals in the commandline parameter are prefixed with an underscore.

| Environment variable | Parameter | Descriptions |
| -------------- | -------------- | ----- |
| EBMS_ADMIN_PROPERTIES_FILE_DIR | -propertiesFilesDir | The directory where the properties file is located. Default is /app/conf.  |
| EBMS_ADMIN_JAR_PATH | n.a. | Full path of the ebms-admin-jar file. Default is ```/app/jar/ebms-admin-2.16.1.jar``` |
| EBMS_EXTRA_JARS | n.a. | Additional jar files to include in the -cp classpath parameter. This allows the inclusion of database client drivers. |
| EBMS_START_EMBEDDED | n.a. | Start the embedded adapter. Specify with any value, e.g. *EBMS_START_EMBEDDED=yes*. |
| EBMS_ADMIN_AUTHENTICATION | -authentication | Enables authentication. Specify with any value, e.b. *EBMS_AUTHENTICATION=yes*. |
| EBMS_ADMIN_CLIENT_AUTHENTICATION | -clientAuthentication | Enables client authentication. Specify with any value, e.g. *EBMS_ADMIN_CLIENT_AUTHENTICATION=yes*. |
| EBMS_ADMIN_CLIENT_CERTIFICATE_HEADER | -clientCertificateHeader | TODO |
| EMBS_ADMIN_CLIENT_TRUST_STORE_PASSWORD | -clientTrustStorePassword | Password for the client trust store and the certificates in the client trust store must be equal. |
| EMBS_ADMIN_CLIENT_TRUST_STORE_PATH | -clientTrustStorePath | Full filepath of the keystore that contains the certificate of the trusted client certificate(s).  |
| EMBS_ADMIN_CLIENT_TRUST_STORE_TYPE | -clientTrustStoreType | Store type, PKS or PKCS12, defaults to PKCS12. |
| EMBS_ADMIN_KEY_STORE_PASSWORD | -keyStorePassword | Password for the keystore. The password of the store and the keys inside the store must be equal. |
| EMBS_ADMIN_KEY_STORE_PATH | -keyStorePath | Full filepath of the keystore that contains the keyst for the soap api and web interface.  |
| EMBS_ADMIN_KEY_STORE_TYPE | -keyStoreType | Store type, PKS or PKCS12, defaults to PKCS12. |
| EBMS_ADMIN_TRUST_STORE_PASSWORD | -trustStorePassword | Password for the trust store and the certificates in the trust store must be equal. |
| EBMS_ADMIN_TRUST_STORE_PATH | -trustStorePath | Full file path of the keystore that contains the trusted clients for the soap adapter and the web gui. |
| EBMS_ADMIN_TRUST_STORE_TYPE | -trustStoreType | Trust store type, default is PKCS12. |
| EBMS_ADMIN_HOST | -host | IP or host the admin console and the soap api listens on.  |
| EBMS_ADMIN_HEADLESS | -headless | Disables the console. Specify with any value, e.g. *EBMS_ADMIN_HEADLESS=yes* |
| EBMS_ADMIN_HSQLDB | -hsqldb | Enable the internal hsql database. Specify with any value, e.g. *EBMS_ADMIN_HSQLDB=yes*. |
| EBMS_ADMIN_HSQLDB_DIR | -hsqldbDir | Specify the directory for the hsql database. It will be created if not present. |
| EBMS_ADMIN_PATH | -path | path for the web interface and soap api. Defaults to empty path. |
| EBMS_ADMIN_PORT | -port | port for the web interface and soap api. |
| EBMS_ADMIN_SOAP | -soap | Enables the soap client API. Specify with any value, e.g. *EBMS_SOAP=yes*. |
| EBMS_SIGNATURE_KEYSTORE_PASSWORD | signature.keystore.password | Password of the store and the key in the store. |
| EBMS_SIGNATURE_KEYSTORE_PATH | signature.keystore.path | Path of key store. |
| EBMS_SIGNATURE_KEYSTORE_TYPE | signature.keystore.type | Store type, PKS or PKCS12, defaults to PKCS12. |
| EBMS_TRUSTSTORE_PASSWORD | truststore.password | Password of the trust store and the certificates in the trust store.  |
| EBMS_TRUSTSTORE_PATH | truststore.path | Path to the trust store of the ebms adapter. It contains the certificates of the trusted third parties and the required root CA and intermediate CA certificates.  |
| EBMS_TRUSTSTORE_TYPE | truststore.type | Store type, PKS or PKCS12, defaults to PKCS12. |
| EBMS_CLIENT_KEYSTORE_PASSWORD | client.keystore.password| Password of the store and the key in the store. |
| EBMS_CLIENT_KEYSTORE_PATH | client.keystore.path| Path of key store. |
| EBMS_CLIENT_KEYSTORE_TYPE | client.keystore.type| Store type, PKS or PKCS12, defaults to PKCS12. |
| EBMS_HOST | ebms.host | Host name or IP address the ebms service will accept connections on. |
| EBMS_JDBC_DRIVER_CLASS_NAME | ebms.jdbc.driverClassName | Name of the jdbc driver class. |
| EBMS_JDBC_PASSWORD | ebms.jdbc.password | Password of the account for the database. |
| EBMS_JDBC_URL | ebms.jdbc.url | Jdbc URL for the database, driver dependent. |
| EBMS_JDBC_USERNAME | ebms.jdbc.username | Username of the account for the database. |
| EBMS_MESSAGE_DELETE_CONTENT_ON_PROCESSED | ebmsMessage.deleteContentOnProcessed | Set to yes if content should be deleted after being processes. The soap API also allows the control of this behavior. |
| EBMS_MESSAGE_STORE_DUPLICATE_CONTENT | ebmsMessage.storeDuplicateContent | Set to yes to store duplicate content. |
| EBMS_MESSAGE_STORE_DUPLICATE | ebmsMessage.storeDuplicate | Set to yes to store duplicate messages. |
| EBMS_PATH | ebms.path | Http path of the ebms service. Defaults to embs |
| EBMS_POOL_PREFERRED_TEST_QUERY | ebms.pool.preferredTestQuery | Query to test if the database is up and running. |
| EBMS_PORT | ebms.port | Port the ebms adapter listens on. Defaults to 8888. |
| EBMS_SSL | ebms.ssl |  |
| EBMS_EVENT_LISTENER_TYPE | eventListener.type |  |
| EBMS_BASE64_WRITER | http.base64Writer |  |
| EBMS_CHUNKED_STREAMING_MODE | http.chunkedStreamingMode |  |
| EBMS_HTTP_CLIENT | http.client | Type of client, APACHE or JAVA |
| EBMS_REQUIRE_CLIENT_AUTHENTICATION | https.requireClientAuthentication | Set to yes to enforce clients to be authenticated. |
| EBMS_HTTPS_VALIDATE | https.validate |  |
| EBMS_HTTPS_VERIFY_HOSTNAMES | https.verifyHostnames |  |
| EBMS_JMS_BROKER_CONFIG | jms.broker.config |  |
| EBMS_JMS_BROKER_START | jms.broker.start |  |
| EBMS_JMS_BROKER_URL | jms.brokerURL |  |
| EBMS_JMS_VIRTUAL_TOPICS | jms.virtualTopics |  |
| EBMS_KEYSTORE_PASSWORD | keystore.password | Password of the store and the key in the store. |
| EBMS_KEYSTORE_PATH | keystore.path | Path of key store. |
| EBMS_KEYSTORE_TYPE | keystore.type | Store type, PKS or PKCS12, defaults to PKCS12. |
| EBMS_LOG4J_FILE | log4j.file |  |
| EBMS_MAX_ITEMS_PER_PAGE | maxItemsPerPage |  |
| EBMS_ENABLE_CLEO_PATCH | patch.cleo.enable  ||
| EBMS_ENABLE_DIGIPOORT_PATCH | patch.digipoort.enable  |  |
| EBMS_ENABLE_ORACLE_PATCH | patch.oracle.enable  |  |
| EBMS_HTTPS_CLIENT_CERTIFICATE_ATHENTICATION | https.clientCertificateAuthentication |   Enables authentication based on the provided client certificate. |

## Paths
The adapter uses the following paths. These can be mapped on volumes to change the settings.

| Path name | Default value | |
| ---- | ---- | ---- |
| Jar path | /app/jar   |  Path that holds the ebms-adapter jar file. Can be read only. |
| Conf path | /app/conf |  Directory that must hold the ebms-admin.properties or the ebms-admin.embedded.properties file. This file will be generated by the entrypoint.sh script. |
|  Secrets path  | /app/secrets | Default location for the keystore files. Each keystore location can be separately configured. This allows the use of secrets in kubernetes |
| bin path | /app/bin | Directory for the entrypoint.sh file |


- **/app/jar** => contains the jar file of the application
- **/app/conf** => contains the configuration files for the adapter, ```ebms-admin.properties``` or ```ebms-admin.embedded.properties``` and 
     the log4j2 configuration file, ```log4j2.xml```
- **/app/bin** => contains the ```entrypoint.sh``` file that takes the environment      variables and changes them to commandline parameters
     

Image aims to be usable as a simple container in Docker, in combination with other containers in docker compose and in pods on Kubernetes.

To realize that two configuration files are part of the image in the conf directory, 
using the standard settings, except for the log4j.xml setting.

## Build the image

The docker image can be built using the make command ```make all``` or the ones shown below.

## Ports

- The admin console runs on port 8080 http://localhost:8080
- SOAP service configured on http://localhost:8080/service
- EbMS service configured on http://localhost:8888/digipoortStub

The ports change when ssl is enabled. 

### Ports SSL

Default port is 8443. This gives the following endpoints:

- The admin console runs on port 8443 https://localhost:8443
- SOAP service configured on https://localhost:8443/service
- EbMS service configured on https://localhost:8888/digipoortStub


These settings can be changed with the environment variables.

## Run container not keeping state

The default command starts the container using the configuration in **/app/conf** directory, writing logs to **/app/logs** and the internal database in **/app/hsqldb**. These directories are inside the container and are not persisted. 

```
docker run --rm --name embs -p 8080:8080 ebms-admin-embedded
```

## Configure with environment variables

The container should be configured using the documented environment variables. This is the preferred way. The startup script overwrites the properties file. If this is not sufficient then you need to adapt the dockerfile.

## Run the container

The [test](test) directory contains a small set of tests file. [test_embedde.sh](test/test_embedded.sh) starts ebms-admin 
with the embedded adapter using the default keystores as provided by the jar. 

### run ebms with embedded adapter with built in hsqldb database

```
docker run --rm --name ebms-embedded-test \
    -p 8080:8080 \
    -p 8888:8888 \
    --env EBMS_ADMIN_SOAP=Y \
    --env EBMS_START_EMBEDDED=Y \
    --env EBMS_ADMIN_HSQLDB=Y \
    --env EBMS_ADMIN_HSQLDB_DIR=/app/localdb \
    ebms-admin
```

### Run with your own keystores

Use the keystores in secrets directory with the **--mount** option.

```
docker run --rm --name ebms-embedded-test \
    -p 8080:8080 \
    -p 8443:8443 \
    -p 8888:8888 \
    --env EBMS_ADMIN_SOAP=Y \
    --env EBMS_START_EMBEDDED=Y \
    --env EBMS_ADMIN_HSQLDB=Y \
    --env EBMS_ADMIN_HSQLDB_DIR=/app/localdb \
    --env EBMS_ADMIN_HEADLESS=true \
    --mount "type=bind,source=/home/peza/DevProjects/ebms-admin-docker/secrets,destination=/app/secrets,readonly" \
    ebms-admin
```

### Run container keeping state on host local volumes

The **EBMS_ADMIN_HSQLDB=Y** and **EBMS_ADMIN_HSQLDB_DIR=/app/localdb** specify that the internal database should be used and the **--mount** flag mapss the directory for the database to a persistent docker volume.

```
docker run --rm --name ebms-embedded-test \
    -p 8443:8443 \
    -p 8888:8888 \
    --env EBMS_ADMIN_SOAP=Y \
    --env EBMS_START_EMBEDDED=Y \
    --env EBMS_ADMIN_HSQLDB=Y \
    --env EBMS_ADMIN_HSQLDB_DIR=/app/localdb \
    --env EBMS_ADMIN_HEADLESS=true \
    --mount 'type=volume,source=embs-localdb,destination=/app/localdb' \
    --env EBMS_ADMIN_SSL=true \
    --env EBMS_SSL=true \
    ebms-admin
```

### Run ebms on postgres database

Run the ebms-admin with embedded adapter, use custom keystores and a PostgreSQL database.

```
docker run --rm --name ebms-embedded-test \
    -p 8080:8080 \
    --mount "type=bind,source=/home/peza/DevProjects/ebms-admin-docker,destination=/app/secrets,readonly" \
    --env EBMS_SOAP=Y \
    --env EBMS_START_EMBEDDED=Y \
    --env EBMS_JDBC_DRIVER_CLASS_NAME=org.postgresql.Driver \
    --env EBMS_JDBC_USERNAME=ebms \
    --env EBMS_JDBC_PASSWORD=secretPassword \
    --env EBMS_JDBC_URL=jdbc:postgresql://10.0.2.15:5432/ebms \
    --env EBMS_POOL_PREFERRED_TEST_QUERY="select 1" \
    ebms-admin:postgres
```

### Container scanning

```
gcloud auth configure-docker eu.gcr.io
```

```
docker tag ebms-admin:2.16.6 eu.gcr.io/cbs-test-ai-vm/ebms-admin:2.16.6
```