# Samples

Run two EBMS adapters, one for digipoort and one for overheid.

Endpoints are defined in the CPA xml files

- overheid: http://localhost:8088/overheidStub
- digipoort: http://localhost:8888/digipoortStub


Pas de config files aan in conf-digipoort en conf-overheid

ebms.path=/digipoortStub

ebms.path=/overheidStub

Opstarten van de containers

Digipoort:

```
docker run --rm --name ebms-digipoort -p 8888:8888 -p 18080:8080 -v $(pwd)/conf-digipoort:/app/conf ebms-admin-embedded
```

Overheid: 

```
docker run --rm --name ebms-digipoort -p 8888:8888 -p 18080:8080 -v $(pwd)/conf-digipoort:/app/conf ebms-admin-embedded
```
