#!/bin/bash

write_cli_param() {
    echo set_cli_param '"${'$1'}"' '"'$2'"' '"'$3'"'
}

write_cli_flag() {
    echo set_cli_flag '"${'$1'}"' '"'$2'"' 
}

write_property() {
    echo set_param_if_exists '"${'$1'}"' '"'$2'"' '"'$3'"'
}

# $1 contains the name of the files with the mappings file
generate_properties_file() {
    while IFS=';' read f x y z
    do
        case $f in
            'PROPERTY')
            write_property $x $y $z
            ;;
            'CLI_PARAM')
            write_cli_param $x $y
            ;;
            'CLI_FLAG')
            write_cli_flag $x $y
            ;;
        esac

        # echo set param '"${'${x}'}"' '"'${y}'"' '"'$z'"'
    done < $1
}

generate_properties_file "./ebms-admin-env-2-properties"
