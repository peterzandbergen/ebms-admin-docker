#!/bin/bash

properties_file_dir="/app/conf/"
properties_file="ebms-admin.properties"
properties_file_embedded="ebms-admin.embedded.properties"


# set_cli_param ENV_VAR -cli_option
set_cli_param() {
    if [ ! -z ${1} ]
    then
        options="${options} ${2} \"${1}\""
    fi
}

# set_cli_flag ENV_VAR -cli_flag
set_cli_flag() {
    if [ ! -z ${1} ]
    then
        options="${options} ${2}"
    fi
}

# helper functions
escape_colon_sed () {
    echo $1 | sed -r "s|(:)|\\\:|g"
}

# Set the property $1 to value $2 and add to file $3
set_param() {
    # echo set_param $1 $2 $3
    local val=$(escape_colon_sed ${2})
    # local val=$2
    echo "${1}=${val}" >> "$3"
    # echo "${1}=${val}" 
}

# set_param_if_exists ENV_VAR properties.entry properties.file.
# NOTE: put the params between quotes "${ENV_VAR}"
set_param_if_exists() {
    if [ ! -z "${1}" ]
    then
        set_param "$2" "$1" "${properties_file_dir}${properties_file}"
    elif [ ! -z "${3}" ]
    then
        set_param "$2" "$3" "${properties_file_dir}${properties_file}"
    fi
}

ebms_admin_jar_path="ebms-admin-2.16.1.jar"
if [ ! -z ${EBMS_ADMIN_JAR_PATH} ]
then
	ebms_admin_jar_path="${EBMS_ADMIN_JAR_PATH}"
fi

ebms_admin_extra_jars=
if [ ! -z ${EBMS_EXTRA_JARS} ]
then
	ebms_admin_jar_path="${ebms_admin_jar_path}:${EBMS_EXTRA_JARS}"
fi

ebms_admin_class="nl.clockwork.ebms.admin.Start"
if [ ! -z ${EBMS_START_EMBEDDED} ]
then
	ebms_admin_class="nl.clockwork.ebms.admin.StartEmbedded"
fi

set_cli_options() {
    set_cli_flag  "${EBMS_ADMIN_SOAP}"                          "-soap"
    set_cli_flag  "${EBMS_ADMIN_AUTHENTICATION}"                "-authentication"
    set_cli_flag  "${EBMS_ADMIN_CLIENT_AUTHENTICATION}"         "-clientAuthentication"
    set_cli_param "${EBMS_ADMIN_CLIENT_CERTIFICATE_HEADER}"     "-clientCertificateHeader"
    set_cli_param "${EBMS_ADMIN_CLIENT_TRUST_STORE_PASSWORD}"   "-clientTrustStorePassword"
    set_cli_param "${EBMS_ADMIN_CLIENT_TRUST_STORE_PATH}"       "-clientTrustStorePath"
    set_cli_param "${EBMS_ADMIN_CLIENT_TRUST_STORE_TYPE}"       "-clientTrustStoreType"
    set_cli_param "${EBMS_ADMIN_KEY_STORE_PASSWORD}"            "-keyStorePassword"
    set_cli_param "${EBMS_ADMIN_KEY_STORE_PATH}"                "-keyStorePath"
    set_cli_param "${EBMS_ADMIN_KEY_STORE_TYPE}"                "-keyStoreType"
    set_cli_param "${EBMS_ADMIN_HOST}"                          "-host"
    set_cli_flag  "${EBMS_ADMIN_JMX}"                           "-jmx"
    set_cli_param "${EBMS_ADMIN_TRUST_STORE_PASSWORD}"          "-trustStorePassword"
    set_cli_param "${EBMS_ADMIN_TRUST_STORE_PATH}"              "-trustStorePath"
    set_cli_param "${EBMS_ADMIN_TRUST_STORE_TYPE}"              "-trustStoreType"
    set_cli_flag  "${EBMS_ADMIN_HEADLESS}"                      "-headless"
    set_cli_flag  "${EBMS_ADMIN_HSQLDB}"                        "-hsqldb"
    set_cli_param "${EBMS_ADMIN_HSQLDB_DIR}"                    "-hsqldbDir"
    set_cli_param "${EBMS_ADMIN_PATH}"                          "-path"
    set_cli_param "${EBMS_ADMIN_PORT}"                          "-port"
    set_cli_flag  "${EBMS_ADMIN_SSL}"                           "-ssl"
}

write_properties_file() {
    # Start the properties file
    echo "# Generated properties on $(date)" > ${properties_file_dir}${properties_file}

    set_param_if_exists "${EBMS_CLIENT_KEYSTORE_PASSWORD}"               "client.keystore.password" 
    set_param_if_exists "${EBMS_CLIENT_KEYSTORE_PATH}"                   "client.keystore.path" 
    set_param_if_exists "${EBMS_CLIENT_KEYSTORE_TYPE}"                   "client.keystore.type" 
    set_param_if_exists "${EBMS_HOST}"                                   "ebms.host"                    "0.0.0.0"
    set_param_if_exists "${EBMS_JDBC_DRIVER_CLASS_NAME}"                 "ebms.jdbc.driverClassName"    "org.hsqldb.jdbcDriver"
    set_param_if_exists "${EBMS_JDBC_PASSWORD}"                          "ebms.jdbc.password"           
    set_param_if_exists "${EBMS_JDBC_URL}"                               "ebms.jdbc.url"                "jdbc:hsqldb:hsql://localhost:9001/ebms"
    set_param_if_exists "${EBMS_JDBC_USERNAME}"                          "ebms.jdbc.username"           "sa"
    set_param_if_exists "${EBMS_POOL_PREFERRED_TEST_QUERY}"              "ebms.pool.preferredTestQuery" "select 1 from information_schema.system_tables"
    set_param_if_exists "${EBMS_PATH}"                                   "ebms.path" "/ebms"
    set_param_if_exists "${EBMS_PORT}"                                   "ebms.port" "8888"
    set_param_if_exists "${EBMS_SSL}"                                    "ebms.ssl" "false"
    set_param_if_exists "${EBMS_MESSAGE_DELETE_CONTENT_ON_PROCESSED}"    "ebmsMessage.deleteContentOnProcessed" 
    set_param_if_exists "${EBMS_MESSAGE_STORE_DUPLICATE_CONTENT}"        "ebmsMessage.storeDuplicateContent" 
    set_param_if_exists "${EBMS_MESSAGE_STORE_DUPLICATE}"                "ebmsMessage.storeDuplicate" 
    set_param_if_exists "${EBMS_EVENT_LISTENER_TYPE}"                    "eventListener.type"           "DEFAULT"
    set_param_if_exists "${EBMS_ENCRYPTION_KEYSTORE_PASSWORD}"           "encryption.keystore.password" 
    set_param_if_exists "${EBMS_ENCRYPTION_KEYSTORE_PATH}"               "encryption.keystore.path" 
    set_param_if_exists "${EBMS_ENCRYPTION_KEYSTORE_TYPE}"               "encryption.keystore.type" 
    set_param_if_exists "${EBMS_BASE64_WRITER}"                          "http.base64Writer" 
    set_param_if_exists "${EBMS_CHUNKED_STREAMING_MODE}"                 "http.chunkedStreamingMode" 
    set_param_if_exists "${EBMS_HTTP_CLIENT}"                            "http.client"                  "DEFAULT"
    set_param_if_exists "${EBMS_HTTP_PROXY_HOST}"                        "http.proxy.host" 
    set_param_if_exists "${EBMS_HTTP_PROXY_NON_PROXY_HOSTS}"             "http.proxy.nonProxyHosts" 
    set_param_if_exists "${EBMS_HTTP_PROXY_PASSWORD}"                    "http.proxy.password" 
    set_param_if_exists "${EBMS_HTTP_PROXY_PORT}"                        "http.proxy.port" 
    set_param_if_exists "${EBMS_HTTP_PROXY_USERNAME}"                    "http.proxy.username" 
    set_param_if_exists "${EBMS_HTTPS_CIPHER_SUITES}"                    "https.cipherSuites" 
    set_param_if_exists "${EBMS_HTTPS_CLIENT_CERTIFICATE_ATHENTICATION}" "https.clientCertificateAuthentication" 
    set_param_if_exists "${EBMS_HTTPS_PROTOCOLS}"                        "https.protocols" 
    set_param_if_exists "${EBMS_REQUIRE_CLIENT_AUTHENTICATION}"          "https.requireClientAuthentication" 
    # set_param_if_exists "${EBMS_HTTPS_VALIDATE}"                         "https.validate" 
    set_param_if_exists "${EBMS_HTTPS_VERIFY_HOSTNAMES}"                 "https.verifyHostnames" 
    set_param_if_exists "${EBMS_JMS_BROKER_CONFIG}"                      "jms.broker.config" "classpath:nl/clockwork/ebms/activemq.xml"
    set_param_if_exists "${EBMS_JMS_BROKER_START}"                       "jms.broker.start" "true"
    set_param_if_exists "${EBMS_JMS_BROKER_URL}"                         "jms.brokerURL" "vm://localhost"
    set_param_if_exists "${EBMS_JMS_VIRTUAL_TOPICS}"                     "jms.virtualTopics" "false"
    set_param_if_exists "${EBMS_KEYSTORE_PASSWORD}"                      "keystore.password" 
    set_param_if_exists "${EBMS_KEYSTORE_PATH}"                          "keystore.path" 
    set_param_if_exists "${EBMS_KEYSTORE_TYPE}"                          "keystore.type" 
    set_param_if_exists "${EBMS_LOG4J_FILE}"                             "log4j.file" 
    set_param_if_exists "${EBMS_MAX_ITEMS_PER_PAGE}"                     "maxItemsPerPage"              "20"
    set_param_if_exists "${EBMS_ENABLE_CLEO_PATCH}"                      "patch.cleo.enable" 
    set_param_if_exists "${EBMS_ENABLE_DIGIPOORT_PATCH}"                 "patch.digipoort.enable" 
    set_param_if_exists "${EBMS_ENABLE_ORACLE_PATCH}"                    "patch.oracle.enable" 
    set_param_if_exists "${EBMS_SIGNATURE_KEYSTORE_PASSWORD}"            "signature.keystore.password" 
    set_param_if_exists "${EBMS_SIGNATURE_KEYSTORE_PATH}"                "signature.keystore.path" 
    set_param_if_exists "${EBMS_SIGNATURE_KEYSTORE_TYPE}"                "signature.keystore.type" 
    set_param_if_exists "${EBMS_TRUSTSTORE_PASSWORD}"                    "truststore.password" 
    set_param_if_exists "${EBMS_TRUSTSTORE_PATH}"                        "truststore.path" 
    set_param_if_exists "${EBMS_TRUSTSTORE_TYPE}"                        "truststore.type" 
    set_param_if_exists "${EBMS_SERVICE_URL}"                            "service.ebms.url"
}

set_cli_options

# Do not use space as field separator.
old_ifs="${IFS}"
IFS='
'

# Write the properties file if none exists.
if [ ! -f "${properties_file_dir}${properties_file}" ] && [ ! -f "${properties_file_dir}${properties_file_embedded}" ]
then
    echo Generating properties file
    write_properties_file
    cp "${properties_file_dir}${properties_file}" "${properties_file_dir}${properties_file_embedded}"
else
    echo Using existing properties file
    ls -l /app/conf
fi

cat_properties() {
    if [ ! -z ${EBMS_START_EMBEDDED} ]
    then
        cat "${properties_file_dir}${properties_file_embedded}"
    else
        cat "${properties_file_dir}${properties_file}"
    fi

}


# Restore the field separator.
IFS="$old_ifs"


if [ ! -z ${EBMS_ADMIN_HSQLDB_DIR} ]
then
    echo Creating hsqldb directory and give appuser access
    mkdir -p "${EBMS_ADMIN_HSQLDB_DIR}"
    chown -R appuser:appuser "${EBMS_ADMIN_HSQLDB_DIR}"
    echo > /dev/null
fi

java_options="-Dlog4j.configurationFile=/app/conf/log4j2.xml"
command="${JAVA_HOME}/bin/java ${java_options} -cp ${ebms_admin_jar_path} ${ebms_admin_class} ${options}"
gosu appuser:appuser bash -c 'whoami && id'
echo Executing: ${command} 
echo Properties settings
cat_properties
echo =============== Starting ebms ===============
exec gosu appuser:appuser ${command}
